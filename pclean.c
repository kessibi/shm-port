#include <stdio.h>
#include <stdlib.h>
#include "port.h"

int main(int argc, char* argv[]) {

  if(argc != 1) {
    fprintf(stderr, "usage: %s\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  // if semaphores/shared mem exist simply delete them
  int id;

  if((id = acceder_sem(128)) != -1) suppr_sem(id);

  if((id = acceder_sem(2)) != -1) suppr_sem(id);

  if((id = acceder_sem(4)) != -1) suppr_sem(id);

  if((id = acceder_sem(10)) != -1) suppr_sem(id);

  if((id = acceder_sem(8)) != -1) suppr_sem(id);

  if((id = acceder_shm(32)) != -1) suppr_shm(id);

  exit(0);
}
