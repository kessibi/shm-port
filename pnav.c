#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include "port.h"

int main(int argc, char* argv[]) {

  int c, ta, td, where = -1, debug = 0, ndocks;
  int semships, shmid, semload, semwrite, semrun, semdock;

  if(argc != 5 || (c = atoi(argv[2])) <= 0 ||
      (ta = atoi(argv[3])) < 0 || (td = atoi(argv[4])) < 0) {

    fprintf(stderr, "usage: %s <nom> <nb_containers> <ta> <td>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  if(getenv("DEBUG_PORT") && atoi(getenv("DEBUG_PORT"))) debug = 1;

  if((semships = acceder_sem(128)) == -1) crier("semget");

  if((semload = acceder_sem(8)) == -1) crier("semget");

  if((semrun = acceder_sem(2)) == -1) crier("semget");

  if((semwrite = acceder_sem(10)) == -1) crier("semget");

  if((semdock = acceder_sem(4)) == -1) crier("semget");

  if((shmid = acceder_shm(32)) == -1) crier("shmget");

  struct port* port = shmat(shmid, (void*)0, 0);
  if(port == (void*) -1) crier("shmat");

  // end of initialization

  if(!port->running) parler(stderr, "Port is closed for the moment.");
  ndocks = port->ndocks;

  if(debug > 1) parler(stdout, "Waiting for port's approval to land");

  // Is any dock free?
  P(semships, 0);

  if(!port->running) parler(stderr, "Port is closed for the moment.");

  // Boats ask for a dock one at a time to prevent concurrency problems
  P(semwrite, 1);

  // trigger dock event in pcap
  port->boat = argv[1][0];
  V(semrun, 0);

  // wait for pcap to be ready to give me an answer
  P(semrun, 2);

  if(!port->running) parler(stderr, "Port is closed for the moment.");

  where = port->where;
  port->ships[where] = port->boat;
  port->where = -1;

  // signal pcap that the boat knows where to dock
  V(semrun, 4);
  V(semwrite, 1);

  // landing
  if(usleep(ta * 1000) == -1) crier("usleep");

  // boat is ready to take intakes
  port->ships[where + 2 * port->ndocks] = 1;
  port->ships[where+ndocks] = c;

  if(debug) parler(stdout, "Occupying dock number %d", where);

  if(debug > 1) parler(stdout, "Waiting for trucks to come ..");

  // wait for first batch of trucks
  P(semload, ndocks + where);

  while(port->ships[where+ndocks]) {

    // is there room on the dock?
    P(semdock, where);

    if(usleep(td * 1000) == -1) crier("usleep");

    if(debug) parler(stdout, "Container %d of dock %d successfully unloaded.",
        port->ships[where+ndocks], where);
    port->ships[where+ndocks]--;

    // tell the truck the container has been unloaded
    V(semload, where);
  }

  port->ships[where + 2* port->ndocks] = 0;
  // leave the dock
  V(semships, 0);
  port->ships[where] = 0;
  // trigger pcap loop
  V(semrun, 0);

  if(shmdt(port) == -1) crier("shmdt");
  if(debug) parler(stdout, "Leaving the dock number %d", where);

  if(debug) parler(stdout, "Leaving the port");

  exit(0);
}
