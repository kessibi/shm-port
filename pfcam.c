#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include "port.h"

int main(int argc, char* argv[]) {

  int c, tc, shmid, semload, semwrite, semrun, semdock;
  int i, where, debug = 0;
  struct port* port;

  if(argc != 4 || (c = atoi(argv[2])) <= 0 ||(tc = atoi(argv[3])) < 0) {
    fprintf(stderr, "usage: %s <v> <c> <tc>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  if(getenv("DEBUG_PORT") && atoi(getenv("DEBUG_PORT"))) debug = 1;

  if((semload = acceder_sem(8)) == -1) crier("semget");
  if((semwrite = acceder_sem(10)) == -1) crier("semget");
  if((semrun = acceder_sem(2)) == -1) crier("semget");
  if((semdock = acceder_sem(4)) == -1) crier("semget");

  if((shmid = acceder_shm(32)) == -1) crier("shmget");

  if((port = shmat(shmid, (void*)0, 0)) == (void*)-1) crier("shmat");

  // only one batch of trucks at a time	
  P(semwrite, 0);

  // triggering the main pcap loop
  port->truck = argv[1][0];
  V(semrun, 0);

  // waiting for pcap to respond
  P(semrun, 3);
  where = port->where;
  port->where = -1;
  // telling pcap the trucks know where to go
  V(semrun, 1);

  V(semwrite, 0);

  if(where == -1) exit(EXIT_FAILURE);

  if(debug) parler(stdout, "Waiting for ship on dock %d to unload", where);

  // tell the boat we're here
  V(semload, port->ndocks + where);

  for(i=0;i<c;i++) {

    // waiting for a container to be on the dock
    P(semload, where);
    // assigning a truck to it
    V(semdock, where);

    if(usleep(1000 * tc) == -1) crier("usleep");
    if(debug) parler(stdout, "Taking care of container %d", i);

  }

  if(debug > 1) parler(stdout, "Trucks leaving dock %d", where);
  if(shmdt(port) == -1) crier("shmdt");

  exit(0);
}
