#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <errno.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "port.h"

void crier(char* str){
	perror(str);
	exit(EXIT_FAILURE);
}

void parler(FILE* flux, char *str, ...) {

	va_list ap;
	va_start (ap, str);
	vfprintf (flux, str, ap);
	fprintf (flux, "\n");
	va_end (ap) ;

	if(fflush(stdout) != 0) crier("fflush");
	if(flux == stderr) exit(EXIT_FAILURE);
}

void suppr_shm(int shmid) {
	if(shmctl(shmid, IPC_RMID, NULL) == -1) crier("shmctl");
}

int libre_sem(int id, int n) {
	int r;
	if((r = semctl(id, n, GETVAL)) == -1) crier("semctl");
	return r;
}

int creer_sem(int proj_id, int n, int val) {
	key_t k;
	int id, i;
	unsigned short tab[n];

	k = ftok("/usr/include/elf.h", proj_id);
	if(k == -1) crier("ftok");

	if((id = semget(k, 1, 1)) != -1) suppr_sem(id);

	id = semget(k, n, IPC_CREAT | 0666);
	if(id == -1) crier("semget");

	if(n == 1) {
		if(semctl(id, 0, SETVAL, val) == -1) crier("semctl setval");
	} else {
		for(i=0;i<n;i++) tab[i] = val;
		if(semctl(id, n, SETALL, tab) == -1) crier("semctl setval");
	}

	return id;
}

void suppr_sem(int id) {
	int r;
	r = semctl(id, 0, IPC_RMID, NULL);
	if(r == -1) crier("semctl delete sem");
}

void P(int id, unsigned short n) {
	struct sembuf s[1] = {{n, -1, 0}};
	if(semop(id, s, 1) == -1) crier("semop P");
}

void V(int id, unsigned short n) {
	struct sembuf s[1] = {{n, 1, 0}};
	if(semop(id, s, 1) == -1) crier("semop V");
}

int creer_shm(int proj_id, int n) {
	key_t k;
	int shmid;
	k = ftok("/usr/include/elf.h", proj_id);
	if(k == -1) crier("ftok");

	if((shmid = shmget(k, 0, 0)) != -1) suppr_shm(shmid);

	if((shmid = shmget(k, n, IPC_CREAT | 0600)) == -1) crier("shmget");

	return shmid;
}

int acceder_shm(int proj_id) {
	key_t k;

	k = ftok("/usr/include/elf.h", proj_id);
	if(k == -1) crier("ftok");

	// return even if == -1    
	// test is done in programs
	return shmget(k, 0, 0);
}

int acceder_sem(int proj_id) {
	key_t k;

	k = ftok("/usr/include/elf.h", proj_id);
	if (k ==  -1) crier("ftok");

	// Return even if == -1
	// test is done in programs
	return semget (k, 0, 0);
}

