#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <unistd.h>
#include "port.h"

#define gotoxy(x,y) printf("\033[%d;%dH", (x), (y))
#define clear() printf("\033[H\033[J")

// self explanatory
void drawboat(int x, int y) {
  gotoxy(x, y);
  if(y%3 == 0) {
    printf(" .  .. \n");
    gotoxy(x+1, y);
    printf("  . o o.o \n");
    gotoxy(x+2, y);
    printf("      ...oo \n");
  }
  else if (y%3 == 1){
    printf(" .  o .. \n");
    gotoxy(x+1, y);
    printf("  .o.o \n");
    gotoxy(x+2, y);
    printf("     o...oo \n");
  } else {
    printf(" .  . o . \n");
    gotoxy(x+1, y);
    printf("  o  o.o \n");
    gotoxy(x+2, y);
    printf("     o...oo \n");
  }
  gotoxy(x+3, y);
  printf("        __[]__ \n");
  gotoxy(x+4, y);
  printf("     __|_o_o_o\\__ ");
  printf("%*c________________________", 22-y, ' ');
  gotoxy(x+5, y);
  printf("     \\\"\"\"\"\"\"\"\"\"\"/ ");
  printf("%*c|", 22-y, ' ');
  gotoxy(x+6, y);
  printf("      \\. ..  . / ");
  printf("%*c| Guillaume's Fine Port", 23-y, ' ');
  putchar('\n');
}

int main(int argc, char* argv[]) {

  if(argc != 1) {
    fprintf(stderr, "usage: %s\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  int semid, shmid, i, j, x = 3;
  struct port* port;

  if((semid = acceder_sem(2)) == -1) crier("semget");

  if((shmid = acceder_shm(32)) == -1) crier("shmget");

  port = (struct port*)shmat(shmid, (void*)0, 0);

  if(port == (void*)-1) crier("shmat");

  clear();

  printf("GUILLAUME'S FINE PORT:\n");
  printf("There is a total of %d %s.\n",port->ndocks,
      port->ndocks == 1 ? "dock" : "docks");

  // loop to have an animation
  for(i=0;i<21;i++) {

    drawboat(x, i+1);
    for(j=0;j<39;j++) putchar('^');
    putchar('|'); putchar('\n');

    for(j=0;j<port->ndocks;j++) {
      if(port->ships[j]) {
        x++;
        if(!port->ships[j+2*port->ndocks]) {
          printf("Boat %c is in the process of landing\n", port->ships[j]);
        } else {
          printf("Dock %d is occupied by boat ", j);
          printf("\x1B[31m%c\x1B[0m", port->ships[j]);
          printf(" with %d containers to unload.\n", port->ships[j+port->ndocks]);
        }
      }
    }

    printf("There are %d free docks ", port->ndocks-x+3);
    printf("out of %d.%*c", port->ndocks, 30, ' ');
    x = 3;
    usleep(1000 * 300);
    printf("%*c\n", 50, ' ');
  }

  if(shmdt(port) == -1) crier("shmdt");

  exit(0);
}
