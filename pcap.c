#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include "port.h"
#include <unistd.h>

int main(int argc, char* argv[]) {
  int n, i, debug = 0, cont = 0;

  if(argc != 2 || (n = atoi(argv[1])) <= 0) {
    fprintf(stderr, "usage: %s <n>\n", argv[0]);
    exit(EXIT_FAILURE);	
  }

  if(getenv("DEBUG_PORT") && atoi(getenv("DEBUG_PORT"))) debug = 1;

  if(debug >= 1) parler(stdout, "GUILLAUME'S FINE PORT\n  --OPENING--");

  //	semships: represents how many free docks are left
  int semships = creer_sem(128, 1, n);
  //	semdock: represents the container room on each dock, it's free on init
  int semdock = creer_sem(4, n, 1);
  //	semrun: used for:
  //	0 --> loop through the main loop in pcap
  //	1 and 3 --> conversation between captain and trucks
  //	2 and 4 --> conversation between captain and boats
  int semrun = creer_sem(2, 5, 0);
  //	semload: for each dock:
  //	- n first ones used to syncrhonize boat and trucks
  //	- n + i ones used for a boat to wait for a truck to come 
  int semload = creer_sem(8, 2*n, 0);
  //	Write into shared memory restraining access
  //	so there are two boats taking the same place
  //	and prevent concurrency issues
  int semwrite = creer_sem(10, 2, 1);
  //	Shared memory between processes
  //	3 * n is for the ships array:
  //		- n for the boats
  //		- n for the containers
  //		- n for the boat state (0 or 1)
  //	6 is for the shared memory to work smoothly
  int shm = creer_shm(32, (3 * n + 5) * sizeof(int));

  struct port* port = (struct port*)shmat(shm, (void*)0, 0);

  if(port == (void*) -1) crier("shmat");

  // Setting up the port before taking any requests 
  port->ndocks = n;
  port->running = 1;
  port->boat = 0;
  port->where = -1;

  for(i=0;i<3*n;i++) {
    port->ships[i] = 0;
  }

  //	Cont represents containers, if there are 1 or more then it is = 1
  while(port->running || cont) {

    // Waiting to be triggered to loop again
    P(semrun, 0);

    // a boat is asking for a dock
    if(port->boat != 0) {
      if(debug > 1) parler(stdout, "Boat %c is asking for a dock", port->boat);

      for(i=0; i<n && port->where == -1;i++) {
        if(port->ships[i] == 0) port->where = i;
      }

      if(debug >= 1) parler(stdout, "Boat %c will take dock %d",
          port->boat, port->where);
      // telling the boat he can retrieve the information
      V(semrun, 2);
      // waiting for boat's response
      P(semrun, 4);
      port->boat = 0;
    }

    // a truck armada is asking where a boat is
    if(port->truck != 0) {
      if(debug > 1) parler(stdout, "Truck looking for %c", port->truck);

      for(i=0; i<n && port->where == -1; i++)
        if(port->ships[i] == port->truck && port->ships[i+2*port->ndocks])
          port->where = i;

      if(debug) parler(stdout, "Truck looking for %c goes on dock %d",
          port->truck, port->where);
      // telling the truck he can retrieve the information
      V(semrun, 3);
      // waiting for trucks' response
      P(semrun, 1);
      port->truck = 0;
    }

    //	free boats that couldn't settle down in the port if port
    //	is not running anymore
    if(!port->running)
      while(libre_sem(semships, 0) < port->ndocks) V(semships, 0);

    // is there any containers in the shared memory?
    cont = 0;	
    for(i=0;i<port->ndocks;i++)
      if(port->ships[i+port->ndocks]) cont = 1;

    if(debug > 1) parler(stdout, "Port running: %s\n",
        port->running == 1 ? "yes" : "no");
  }

  if(debug) parler(stdout, "  --CLOSING--\n");

  if(shmdt(port) == -1) crier("shmdt");

  //	remove IPC system V semaphores and shared memory
  if(debug > 1) parler(stdout, "Deleting semaphores and shared memory used\n");
  suppr_sem(semships);
  suppr_sem(semdock);
  suppr_sem(semload);
  suppr_sem(semrun);
  suppr_sem(semwrite);

  suppr_shm(shm);

  exit(0);
}
