#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include "port.h"

int main(int argc, char* argv[]) {

  if(argc != 1) {
    fprintf(stderr, "usage: %s\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  int semrun, shm, debug = 0;

  if(getenv("DEBUG_PORT") && atoi(getenv("DEBUG_PORT"))) debug = 1;

  if((semrun = acceder_sem(2)) == -1) crier("semget");

  if((shm = acceder_shm(32)) == -1) crier("semget");

  if(debug) parler(stdout, "Port needs to close");

  struct port* port = shmat(shm, (void*)0, 0);

  if(debug) parler(stdout, "pstop is asking for the port to close");

  // Port running state goes from 1 to 0
  port->running = 0;
  V(semrun, 0);

  if(shmdt(port)) crier("shmdt");

  exit(0);
}
