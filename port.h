#ifndef __PORT
#define __PORT

struct port {
	int ndocks, running, boat, truck, where;	
	int ships[];
};

void crier(char* str);

void parler(FILE* flux, char* str, ...);

int creer_shm(int proj_id, int n);

int acceder_shm(int proj_id);

void suppr_shm(int shmid);

int creer_sem(int proj_id, int n, int val);

int libre_sem(int id, int n);

int acceder_sem(int proj_id);

void suppr_sem(int id);

void P(int id, unsigned short n);

void V(int id, unsigned short n);

void dock_nb(struct port* p); 

void whereisboat(struct port* p); 

#endif
