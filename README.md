# shm-port

Simulation of a port using IPC SystemV semaphores and shard memory.
The port can be found in pcap, a boat will be an instance of pnav, and trucks
in pfcam.

Once the port is started it accepts boats and brings them to a free dock.
If no dock is free, the boat will wait at sea for a dock to free.

Trucks looking for boat x will be directed to the dock where the boat x
currently is.

## How to use

`make` to compile everything.

`./pcap` to open the port.

`./pnav` to simulate a ship.

`./pfcam` to simulate a truck armada.

Running `./pstop` will tell the port to close.

`./pdump` for extra informations about the current state of the port.

`make clean`


## Context

This project was made during my fifth semester at University.
It was for the class 'Concurrent Systems' thaught by Pierre David.